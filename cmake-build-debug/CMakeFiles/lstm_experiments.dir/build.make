# CMAKE generated file: DO NOT EDIT!
# Generated by "MinGW Makefiles" Generator, CMake Version 3.12

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

SHELL = cmd.exe

# The CMake executable.
CMAKE_COMMAND = "C:\Program Files\JetBrains\CLion 2018.2.4\bin\cmake\win\bin\cmake.exe"

# The command to remove a file.
RM = "C:\Program Files\JetBrains\CLion 2018.2.4\bin\cmake\win\bin\cmake.exe" -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = E:\Projects\cpp\lstm-experiments

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = E:\Projects\cpp\lstm-experiments\cmake-build-debug

# Include any dependencies generated for this target.
include CMakeFiles/lstm_experiments.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/lstm_experiments.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/lstm_experiments.dir/flags.make

CMakeFiles/lstm_experiments.dir/main.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/main.cpp.obj: ../main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/lstm_experiments.dir/main.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\main.cpp.obj -c E:\Projects\cpp\lstm-experiments\main.cpp

CMakeFiles/lstm_experiments.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/main.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\main.cpp > CMakeFiles\lstm_experiments.dir\main.cpp.i

CMakeFiles/lstm_experiments.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/main.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\main.cpp -o CMakeFiles\lstm_experiments.dir\main.cpp.s

CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.obj: ../lib/words2vec/Trainer.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\words2vec\Trainer.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\words2vec\Trainer.cpp

CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\words2vec\Trainer.cpp > CMakeFiles\lstm_experiments.dir\lib\words2vec\Trainer.cpp.i

CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\words2vec\Trainer.cpp -o CMakeFiles\lstm_experiments.dir\lib\words2vec\Trainer.cpp.s

CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.obj: ../lib/lstm/ForwardPass.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\lstm\ForwardPass.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\lstm\ForwardPass.cpp

CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\lstm\ForwardPass.cpp > CMakeFiles\lstm_experiments.dir\lib\lstm\ForwardPass.cpp.i

CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\lstm\ForwardPass.cpp -o CMakeFiles\lstm_experiments.dir\lib\lstm\ForwardPass.cpp.s

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.obj: ../lib/dnn-tools/ActivationFunctions.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\ActivationFunctions.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\dnn-tools\ActivationFunctions.cpp

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\dnn-tools\ActivationFunctions.cpp > CMakeFiles\lstm_experiments.dir\lib\dnn-tools\ActivationFunctions.cpp.i

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\dnn-tools\ActivationFunctions.cpp -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\ActivationFunctions.cpp.s

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.obj: ../lib/dnn-tools/DnnOperations.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnOperations.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnOperations.cpp

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnOperations.cpp > CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnOperations.cpp.i

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnOperations.cpp -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnOperations.cpp.s

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.obj: ../lib/dnn-tools/DnnPersistence.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnPersistence.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnPersistence.cpp

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnPersistence.cpp > CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnPersistence.cpp.i

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnPersistence.cpp -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnPersistence.cpp.s

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.obj: CMakeFiles/lstm_experiments.dir/flags.make
CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.obj: ../lib/dnn-tools/DnnUtils.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Building CXX object CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.obj"
	D:\mingw\bin\g++.exe  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnUtils.cpp.obj -c E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnUtils.cpp

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.i"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnUtils.cpp > CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnUtils.cpp.i

CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.s"
	D:\mingw\bin\g++.exe $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S E:\Projects\cpp\lstm-experiments\lib\dnn-tools\DnnUtils.cpp -o CMakeFiles\lstm_experiments.dir\lib\dnn-tools\DnnUtils.cpp.s

# Object files for target lstm_experiments
lstm_experiments_OBJECTS = \
"CMakeFiles/lstm_experiments.dir/main.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.obj" \
"CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.obj"

# External object files for target lstm_experiments
lstm_experiments_EXTERNAL_OBJECTS =

lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/main.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/words2vec/Trainer.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/lstm/ForwardPass.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/dnn-tools/ActivationFunctions.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnOperations.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnPersistence.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/lib/dnn-tools/DnnUtils.cpp.obj
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/build.make
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/linklibs.rsp
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/objects1.rsp
lstm_experiments.exe: CMakeFiles/lstm_experiments.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Linking CXX executable lstm_experiments.exe"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles\lstm_experiments.dir\link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/lstm_experiments.dir/build: lstm_experiments.exe

.PHONY : CMakeFiles/lstm_experiments.dir/build

CMakeFiles/lstm_experiments.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles\lstm_experiments.dir\cmake_clean.cmake
.PHONY : CMakeFiles/lstm_experiments.dir/clean

CMakeFiles/lstm_experiments.dir/depend:
	$(CMAKE_COMMAND) -E cmake_depends "MinGW Makefiles" E:\Projects\cpp\lstm-experiments E:\Projects\cpp\lstm-experiments E:\Projects\cpp\lstm-experiments\cmake-build-debug E:\Projects\cpp\lstm-experiments\cmake-build-debug E:\Projects\cpp\lstm-experiments\cmake-build-debug\CMakeFiles\lstm_experiments.dir\DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/lstm_experiments.dir/depend

