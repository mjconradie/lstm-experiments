//
// Created by Marius Conradie on 2018/10/22.
//
#include "DnnOperations.h"

//////////////////////////////////////////////////
// Helper methods for applying operations to matrices
Eigen::MatrixXf DNNTOOLS::DnnOperations::applyAsMatrix(
        const std::pointer_to_unary_function<float, float> &activatorFunction,
        const Eigen::MatrixXf X) {
    return X.unaryExpr(activatorFunction);
}

Eigen::VectorXf DNNTOOLS::DnnOperations::applyAsVector(
        const std::pointer_to_unary_function<float, float> &activatorFunction,
        const Eigen::VectorXf x) {
    return x.unaryExpr(activatorFunction);
}