//
// Created by Marius Conradie on 2018/10/22.
//

#ifndef LSTM_EXPERIMENTS_ACTIVATIONFUNCTIONS_H
#define LSTM_EXPERIMENTS_ACTIVATIONFUNCTIONS_H

namespace DNNTOOLS {
    class ActivationFunctions {
    public:
        static float sigmoid(float x);

        static float hyperbolicTangent(float x);

        static float linear(float x);
    };
}
#endif //LSTM_EXPERIMENTS_ACTIVATIONFUNCTIONS_H
