//
// Created by Marius Conradie on 2018/10/24.
//

#ifndef LSTM_EXPERIMENTS_DNNPERSISTANCE_H
#define LSTM_EXPERIMENTS_DNNPERSISTANCE_H


#include <fstream>
#include "../third-party/Eigen/Dense"
//#include "../third-party/eigen3-hdf5/eigen3-hdf5.hpp"

using namespace Eigen;
using namespace std;

class DnnPersistence {
public:
    template<class Matrix>
    inline static void saveMatrix(const char *filename, const Matrix& matrix){
        ofstream out(filename,ios::out | ios::binary | ios::trunc);
        Index rows=matrix.rows(), cols=matrix.cols();
        out.write((char*) (&rows), sizeof(typename Matrix::Index));
        out.write((char*) (&cols), sizeof(typename Matrix::Index));
        out.write((char*) matrix.data(), rows*cols*sizeof(typename Matrix::Scalar) );
        out.close();
    }

    template<class Matrix>
    inline static const void loadAsMatrix(const char *filename, Matrix& matrix) {
        std::ifstream in(filename, ios::in | std::ios::binary);
        typename Matrix::Index rows = 0, cols = 0;
        in.read((char *) (&rows), sizeof(typename Matrix::Index));
        in.read((char *) (&cols), sizeof(typename Matrix::Index));
        matrix.resize(rows, cols);
        in.read((char *) matrix.data(), rows * cols * sizeof(typename Matrix::Scalar));
        in.close();
    }

    static void importWordVectors(const char *importFileName, const char *exportFileName,
                                long long vectorDimensions, bool skipHeader);

};

namespace Eigen{

    template<class Matrix>
    void read_binary(const char* filename, Matrix& matrix){
        std::ifstream in(filename,ios::in | std::ios::binary);
        typename Matrix::Index rows=0, cols=0;
        in.read((char*) (&rows),sizeof(typename Matrix::Index));
        in.read((char*) (&cols),sizeof(typename Matrix::Index));
        matrix.resize(rows, cols);
        in.read( (char *) matrix.data() , rows*cols*sizeof(typename Matrix::Scalar) );
        in.close();
    }
} // Eigen::

#endif //LSTM_EXPERIMENTS_DNNPERSISTANCE_H
