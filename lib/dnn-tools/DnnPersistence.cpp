//
// Created by Marius Conradie on 2018/10/24.
//

#include <iostream>
#include "DnnPersistence.h"

void DnnPersistence::importWordVectors(const char *importFileName, const char *exportFileName,
                                        long long vectorDimensions, bool skipHeader) {

    ifstream insFile(importFileName, ios::in);
    string line;
    long long processed = 0L;
    long long skipped = 0L;
    long long wordIndex = 0L;

    while (getline(insFile, line)) {
            processed++;
            cout << line << endl;
    }

    insFile.close();

    cout << processed << " lines processed, " << skipped << " lines skipped." << endl;
//    ofstream out(filename,ios::out | ios::binary | ios::trunc);
}
