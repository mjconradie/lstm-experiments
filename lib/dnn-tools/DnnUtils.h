//
// Created by Marius Conradie on 2018/10/24.
//

#ifndef LSTM_EXPERIMENTS_DNNUTILS_H
#define LSTM_EXPERIMENTS_DNNUTILS_H

#include <iostream>
#include "../third-party/Eigen/Dense"

using namespace Eigen;
using namespace std;

class DnnUtils {
public:
    inline static void displayVector(const VectorXf &vector, const char *displaylabel) {
        IOFormat HeavyFmt(FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
        cout << endl << "\t\t" << displaylabel << endl << vector.format(HeavyFmt) << endl;
    }

    inline static void displayMatrix(const MatrixXf &matrix, const char *displaylabel) {
        IOFormat HeavyFmt(FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
        cout << endl << "\t\t" << displaylabel << endl << matrix.format(HeavyFmt) << endl;
    }
};


#endif //LSTM_EXPERIMENTS_DNNUTILS_H
