//
// Created by Marius Conradie on 2018/10/22.
//

#ifndef LSTM_EXPERIMENTS_DNNOPERATIONS_H
#define LSTM_EXPERIMENTS_DNNOPERATIONS_H

#include <functional>
#include "../third-party/Eigen/Dense"

using namespace Eigen;
using namespace std;

namespace DNNTOOLS {
    class DnnOperations {
    public:
       static Eigen::MatrixXf applyAsMatrix(const std::pointer_to_unary_function<float, float> &activatorFunction,
                                            const Eigen::MatrixXf X);
       static Eigen::VectorXf applyAsVector(const std::pointer_to_unary_function<float, float> &activatorFunction,
                                             const Eigen::VectorXf x);
    };
}

#endif //LSTM_EXPERIMENTS_DNNOPERATIONS_H
