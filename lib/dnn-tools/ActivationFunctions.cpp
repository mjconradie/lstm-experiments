//
// Created by Marius Conradie on 2018/10/22.
//

#include <cmath>
#include "ActivationFunctions.h"

//////////////////////////////////////////////////
// Activator functions
//
float DNNTOOLS::ActivationFunctions::sigmoid(float x) {
    return 1 / (1 + exp(-x));
}

float DNNTOOLS::ActivationFunctions::hyperbolicTangent(float x) {
    return (1 - exp(-2 * x)) / (1 + exp(-2 * x));
}

float DNNTOOLS::ActivationFunctions::linear(float x) {
    return x;
}