//
// Created by Marius Conradie on 2018/10/22.
//

#ifndef LSTM_EXPERIMENTS_FORWARDPASS_H
#define LSTM_EXPERIMENTS_FORWARDPASS_H

#include "../third-party/Eigen/Dense"
#include "../dnn-tools/DnnOperations.h"
#include "../dnn-tools/ActivationFunctions.h"

using namespace Eigen;
using namespace DNNTOOLS;


namespace LSTM {
    class ForwardPass {

    public:
        ForwardPass(long long inputParameterSize, long long outputParameterSize):
                                hypothesisParameterSize(outputParameterSize),
                                inputParameterSize(inputParameterSize) {
            Wf = MatrixXf::Random(this->hypothesisParameterSize, this->inputParameterSize);
            Wi = MatrixXf::Random(this->hypothesisParameterSize, this->inputParameterSize);
            Wo = MatrixXf::Random(this->hypothesisParameterSize, this->inputParameterSize);
            Wc = MatrixXf::Random(this->hypothesisParameterSize, this->inputParameterSize);

            Uf = MatrixXf::Random(this->hypothesisParameterSize, this->hypothesisParameterSize);
            Ui = MatrixXf::Random(this->hypothesisParameterSize, this->hypothesisParameterSize);
            Uo = MatrixXf::Random(this->hypothesisParameterSize, this->hypothesisParameterSize);
            Uc = MatrixXf::Random(this->hypothesisParameterSize, this->hypothesisParameterSize);

            x = VectorXf::Zero(this->inputParameterSize);
            h = VectorXf::Zero(this->hypothesisParameterSize);
            f = VectorXf::Zero(this->hypothesisParameterSize);
            bf = VectorXf::Random(this->hypothesisParameterSize);
            i = VectorXf::Zero(this->hypothesisParameterSize);
            bi = VectorXf::Random(this->hypothesisParameterSize);
            o = VectorXf::Zero(this->hypothesisParameterSize);
            bo = VectorXf::Random(this->hypothesisParameterSize);
            c = VectorXf::Zero(this->hypothesisParameterSize);
            bc = VectorXf::Random(this->hypothesisParameterSize);

            // for t=0: h(t=0) and c(t=0) are zeroed
            previous_c = VectorXf::Zero(this->hypothesisParameterSize);
            previous_h = VectorXf::Zero(this->hypothesisParameterSize);

        };

        VectorXf predict(const VectorXf &inputFeatures);

        const MatrixXf &getWf() const {
            return Wf;
        }

        void setWf(const MatrixXf &Wf) {
            ForwardPass::Wf = Wf;
        }

        const MatrixXf &getWi() const {
            return Wi;
        }

        void setWi(const MatrixXf &Wi) {
            ForwardPass::Wi = Wi;
        }

        const MatrixXf &getWo() const {
            return Wo;
        }

        void setWo(const MatrixXf &Wo) {
            ForwardPass::Wo = Wo;
        }

        const MatrixXf &getWc() const {
            return Wc;
        }

        void setWc(const MatrixXf &Wc) {
            ForwardPass::Wc = Wc;
        }

        const MatrixXf &getUf() const {
            return Uf;
        }

        void setUf(const MatrixXf &Uf) {
            ForwardPass::Uf = Uf;
        }

        const MatrixXf &getUi() const {
            return Ui;
        }

        void setUi(const MatrixXf &Ui) {
            ForwardPass::Ui = Ui;
        }

        const MatrixXf &getUo() const {
            return Uo;
        }

        void setUo(const MatrixXf &Uo) {
            ForwardPass::Uo = Uo;
        }

        const MatrixXf &getUc() const {
            return Uc;
        }

        void setUc(const MatrixXf &Uc) {
            ForwardPass::Uc = Uc;
        }

        const VectorXf &getX() const {
            return x;
        }

        void setX(const VectorXf &x) {
            ForwardPass::x = x;
        }

        const VectorXf &getH() const {
            return h;
        }

        void setH(const VectorXf &h) {
            ForwardPass::h = h;
        }

        const VectorXf &getBf() const {
            return bf;
        }

        void setBf(const VectorXf &bf) {
            ForwardPass::bf = bf;
        }

        const VectorXf &getBi() const {
            return bi;
        }

        void setBi(const VectorXf &bi) {
            ForwardPass::bi = bi;
        }

        const VectorXf &getBo() const {
            return bo;
        }

        void setBo(const VectorXf &bo) {
            ForwardPass::bo = bo;
        }

        const VectorXf &getBc() const {
            return bc;
        }

        void setBc(const VectorXf &bc) {
            ForwardPass::bc = bc;
        }

        const VectorXf &getC() const {
            return c;
        }

        void setC(const VectorXf &c) {
            ForwardPass::c = c;
        }

        const VectorXf &getF() const {
            return f;
        }

        void setF(const VectorXf &f) {
            ForwardPass::f = f;
        }

        const VectorXf &getO() const {
            return o;
        }

        void setO(const VectorXf &o) {
            ForwardPass::o = o;
        }

        const VectorXf &getI() const {
            return i;
        }

        void setI(const VectorXf &i) {
            ForwardPass::i = i;
        }

        const VectorXf &getPrevious_c() const {
            return previous_c;
        }

        void setPrevious_c(const VectorXf &previous_c) {
            ForwardPass::previous_c = previous_c;
        }

        const VectorXf &getPrevious_h() const {
            return previous_h;
        }

        void setPrevious_h(const VectorXf &previous_h) {
            ForwardPass::previous_h = previous_h;
        }

    private:
        long long hypothesisParameterSize; // 8 bytes long to accomodate very large networks
        long long inputParameterSize;
        MatrixXf Wf, Wi, Wo, Wc, Uf, Ui, Uo, Uc;
        VectorXf x, h, bf, bi, bo, bc, c, f, o, i,
                 previous_c, previous_h;

    };
}


#endif //LSTM_EXPERIMENTS_FORWARDPASS_H
