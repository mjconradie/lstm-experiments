//
// Created by Marius Conradie on 2018/10/22.
//

#include <cmath>
#include <functional>
#include "ForwardPass.h"


VectorXf LSTM::ForwardPass::predict(const VectorXf &inputFeatures) {
    this->setX(inputFeatures);

    VectorXf sumF = this->getWf() * inputFeatures + this->getUf() * this->getPrevious_h() + this->getBf();
    this->setF(DnnOperations::applyAsVector(ptr_fun(ActivationFunctions::sigmoid), sumF));

    VectorXf sumI = this->getWi() * inputFeatures + this->getUi() * this->getPrevious_h() + getBi();
    this->setI(DnnOperations::applyAsVector(ptr_fun(ActivationFunctions::sigmoid), sumI));

    VectorXf sumO = this->getWo() * inputFeatures + this->getUo() * this->getPrevious_h() + this->getBo();
    this->setO(DnnOperations::applyAsVector(ptr_fun(ActivationFunctions::sigmoid), sumO));

    VectorXf sumCI = this->getWc() * inputFeatures + this->getUc() * this->getPrevious_h() + this->getBc();
    VectorXf hyperCI = DnnOperations::applyAsVector(ptr_fun(ActivationFunctions::hyperbolicTangent), sumCI);
    // Now perform coefficient wise multiplication, Hadamard product
    VectorXf sumC = this->getF().array() * this->getPrevious_c().array() + this->getI().array() * hyperCI.array();
    this->setC(sumC);

    this->setH(this->getO().array() * DnnOperations::applyAsVector(ptr_fun(ActivationFunctions::linear), this->getC()).array());

    this->setPrevious_c(this->getC());
    this->setPrevious_h(this->getH());

    return this->getH();
}
