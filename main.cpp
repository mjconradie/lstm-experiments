#include <iostream>
#include <time.h>
#include "lib/third-party/Eigen/Dense"
#include "lib/lstm/ForwardPass.h"
#include "lib/dnn-tools/ActivationFunctions.h"
#include "lib/dnn-tools/DnnOperations.h"
#include "lib/dnn-tools/DnnPersistence.h"
#include "lib/dnn-tools/DnnUtils.h"

void displayInputOutput(const VectorXf &input, const VectorXf &output);
void displayLSTMState(const LSTM::ForwardPass &lstm, const string &label, int iterationCount);


using namespace DNNTOOLS;
using namespace Eigen;
using namespace std;

void exampleInitRuntime( long rows, long cols) {
    MatrixXd m = MatrixXd::Random(rows,cols);
    m = (m + MatrixXd::Constant(rows,cols,1.2)) * 50;
    cout << "m =" << endl << m << endl;
    VectorXd v(rows);

    for (int i = 0; i < rows; i++) {
//        v << i;
        v(i) = i;
    }

    cout << "v =" << endl << v << endl;

    cout << "m * v =" << endl << m * v << endl;
}

void transpose(long rows, long cols) {
    MatrixXf W(rows, cols);

    cout << "W (initial): " << endl << W << endl;

    W = (W + MatrixXf::Constant(rows, cols, 1.2f)) * 50;

    cout << "W (init with Constants):" << endl << W << endl;

    MatrixXf Wt(rows, cols);

    Wt = W.transpose();
    cout << "W (init with Constants):" << endl << W << endl;
    cout << "W_transposed (init with Constants):" << endl << Wt << endl;



}

void testLSTMActivatorFunctions() {
    MatrixXf W = MatrixXf::Random(3,5);

    cout << "W is " << endl << W << endl;

    cout << "sigmoid act.fn(0.98f) = " << ActivationFunctions::sigmoid(0.98f) << endl;

    MatrixXf sigmoidW = DnnOperations::applyAsMatrix(ptr_fun(ActivationFunctions::sigmoid), W);

    cout << "sigmoid(W) = " << endl << sigmoidW << endl;

    cout << "hyperbolic tangent act.fn(0.98f) = " << endl << ActivationFunctions::hyperbolicTangent(0.98f) << endl;

    MatrixXf hyperbolicW = DnnOperations::applyAsMatrix(ptr_fun(ActivationFunctions::hyperbolicTangent), W);

    cout << "hyperbolicTangent(W) = " << endl << hyperbolicW << endl;

    cout << "linear act.fn(0.98f) = " << endl << ActivationFunctions::linear(0.98f) << endl;

    cout << "linear(W) = " << endl << DnnOperations::applyAsMatrix(ptr_fun(ActivationFunctions::linear), W) << endl;

    if (W == DnnOperations::applyAsMatrix(ptr_fun(ActivationFunctions::linear), W)) {
        cout << "W is same as linear(W)" << endl;
    }

    if (W == sigmoidW) {
        cerr << "W is same as sigmoid(W)" << endl;
    }

    if (W == hyperbolicW) {
        cerr << "W is same as hyperbolic(W)" << endl;
    }

    if (sigmoidW == hyperbolicW) {
        cerr << "sigmoid(W) is same as hyperbolic(W)" << endl;
    }


}

void testLSTM() {
    const long long INPUT_SIZE = 3L;
    const long long OUTPUT_SIZE = 2L;
    LSTM::ForwardPass lstm(INPUT_SIZE, OUTPUT_SIZE);

//    displayLSTMState(lstm, "Testing initialization for state ", 0);


    VectorXf input(INPUT_SIZE);
    input.setOnes();
    VectorXf h(OUTPUT_SIZE);

    for (int idx = 0; idx < 3; idx++) {
        // don't modify the initial condition, part of testing the code correctness
        if (idx > 0) {
            h = lstm.predict(input);
        }
        displayLSTMState(lstm, "Testing initialization for state ", idx);
        displayInputOutput(input, h);
    }

    // Test persistence

    DnnPersistence::saveMatrix("test.mat", lstm.getWc());

    MatrixXf m;

    DnnPersistence::loadAsMatrix("test.mat", m);

    DnnUtils::displayMatrix(lstm.getWc(), "Stored Matrix");

    DnnUtils::displayMatrix(m, "Restored (Loaded) Matrix");



//    displayLSTMState(lstm, "Testing initialization for state 1");
//
//    cout << << "Input: " << endl << input << endl << "predict: " << endl << h << endl;
//
//    h = lstm.predict(input);
//
//    displayLSTMState(lstm, "Testing initialization for state 2");
//
//    cout << endl << "\t\tInput: " << endl << input << endl << "\t\tmodel: " << endl << h << endl;


}

void testStoringAndLoadingHUGEmatrix() {
    clock_t start = clock();
    long long ROWS = 5 * 1024 * 1024; // 100 million words / input features / words
    long long COLS = 300; // 300 parameters per word.
    MatrixXf hugeMatrix = MatrixXf::Random(ROWS, COLS);
    clock_t startSave = clock();
    DnnPersistence::saveMatrix("large_test.mat", hugeMatrix);
    MatrixXf loadedMatrix;
    clock_t saved = clock();

    DnnPersistence::loadAsMatrix("large_test.mat", loadedMatrix);
    clock_t loaded = clock();
    long long matches = 0L, mismatches = 0L;

    for (long long row = 0L; row < ROWS; row++) {
        for (long long col = 0L; col < COLS; col++) {
            if (hugeMatrix(row, col) == loadedMatrix(row, col)) {
                matches++;
            } else {
                mismatches++;
            }
        }
    }

    clock_t end = clock();

    cout << "Huge Matrix, " << ROWS << " rows (input words) x " <<  COLS << " cols (word features): "
        << matches << " coeffients matched. " << mismatches
        << " coeffients mismatched after loading matrix from file." << endl;

    cout << "To generate matrix variable/data: " << startSave - start << endl
        << "To save matrix: " << saved - startSave << endl
        << "To load matrix: " << loaded - saved << endl
        << "To loop through matrix: " << end - loaded << endl;
}



int main() {
    const long TOTAL_ROWS = 10L;
    const long TOTAL_COLUMNS = 10L;

    cout << "\tQuick reload test after app end cycle" << endl;
    MatrixXf m;

    DnnPersistence::loadAsMatrix("test.mat", m);

    DnnUtils::displayMatrix(m, "Restored (Loaded) Matrix");


    cout << "\tLSTM Experiments" << endl;

    testLSTM();


    cout << "\n\nImport word vector file" << endl;

    DnnPersistence::importWordVectors("../../data/wiki.simple.vec", "../../data/wiki.simple.mat");


//    cout << "\tTesting Large/dense matrix persistence" << endl;
//    testStoringAndLoadingHUGEmatrix();

//    MatrixXd m(2,2);
//
//    m(0,0) = 3;
//    m(1,0) = 2.5;
//    m(0,1) = -1;
//    m(1,1) = m(1,0) + m(0,1);
//    cout << "m is " << endl << m << endl;

//    testLSTMActivatorFunctions();

//    cout << "Runtime init example: " << endl;

//    exampleInitRuntime(TOTAL_ROWS, TOTAL_COLUMNS);
//    transpose(TOTAL_ROWS, TOTAL_COLUMNS);

    return 0;
}

/* #include <iostream>
#include <Eigen/Dense>
using MatrixXd;
int main()
{
  MatrixXd m(2,2);
  m(0,0) = 3;
  m(1,0) = 2.5;
  m(0,1) = -1;
  m(1,1) = m(1,0) + m(0,1);
  cout << m << endl;
}*/

void displayInputOutput(const VectorXf &input, const VectorXf &output) {
    IOFormat HeavyFmt(FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
    cout << endl << "\t\tx: " << endl << input.format(HeavyFmt) << endl
         << "\t\th: " << endl << output.format(HeavyFmt) << endl;
}

void displayLSTMState(const LSTM::ForwardPass &lstm, const string &label, int iterationCount) {
    IOFormat HeavyFmt(FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");
    cout << endl << "\t\t------[ " << label << iterationCount << " ]-----" << endl << endl;
    cout << "Wf = " << endl << lstm.getWf().format(HeavyFmt) << endl;
    cout << "Wi = " << endl << lstm.getWi().format(HeavyFmt) << endl;
    cout << "Wo = " << endl << lstm.getWo().format(HeavyFmt) << endl;
    cout << "Wc = " << endl << lstm.getWc().format(HeavyFmt) << endl;

    cout << "Uf = " << endl << lstm.getUf().format(HeavyFmt) << endl;
    cout << "Ui = " << endl << lstm.getUi().format(HeavyFmt) << endl;
    cout << "Uo = " << endl << lstm.getUo().format(HeavyFmt) << endl;
    cout << "Uc = " << endl << lstm.getUc().format(HeavyFmt) << endl;

    cout << "x = " << endl << lstm.getX().format(HeavyFmt) << endl;
    cout << "i = " << endl << lstm.getI().format(HeavyFmt) << endl;
    cout << "bi = " << endl << lstm.getBi().format(HeavyFmt) << endl;

    cout << "f = " << endl << lstm.getF().format(HeavyFmt) << endl;
    cout << "bf = " << endl << lstm.getBf().format(HeavyFmt) << endl;

    cout << "o = " << endl << lstm.getO().format(HeavyFmt) << endl;
    cout << "bo = " << endl << lstm.getBo().format(HeavyFmt) << endl;
    cout << "c = " << endl << lstm.getC().format(HeavyFmt) << endl;
    cout << "bc = " << endl << lstm.getBc().format(HeavyFmt) << endl;
    cout << "h = " << endl << lstm.getH().format(HeavyFmt) << endl;

    cout << "C0 = " << endl << lstm.getPrevious_c().format(HeavyFmt) << endl;
    cout << "H0 = " << endl << lstm.getPrevious_h().format(HeavyFmt) << endl;
}